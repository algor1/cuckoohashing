/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */

package com.mycompany.cuckoohashing;

/**
 *
 * @author ASUS
 */
public class CuckooHashing {
    private int[] H1= new int[11];
    private int[] H2 = new int[11];
    
    public  int hash1(int key) {
        return key%H1.length;
    }
    public  int hash2(int key) {
        return (key/11)%H2.length;
    }
     public int getH1(int key){
        return H1[hash1(key)];
    }  
     public int getH2(int key){
        return H2[hash2(key)];
    }  
     
     public void h1(int key){
        int x = hash1(key);
        if(getH1(key) == 0){
            H1[x]=key;
        }else{
            h2(getH1(key));
            H1[x]=key;
        }
     }
     
     public void h2(int key){
         int x = hash2(key);
         if(getH2(key) ==  0){
             H2[x]=key;
         }else{
             h1(getH2(key));
             H2[x]=key;
         }
     }
    public void put(int key){
        h1(key);
    }
    public static void main(String[] args) {
        CuckooHashing c = new CuckooHashing();
        int [] arr ={20, 50, 53, 75, 100, 67, 105, 3, 36};
        for(int i = 0 ; i < arr.length ; i++){
            c.put(arr[i]);
        }

//        c.put(20);
//        c.put(50);
//        c.put(53);
//        c.put(75);
//        c.put(100);
//        c.put(67);
//        c.put(105);
//        c.put(3);
//        c.put(36);
        for(int i = 0 ; i <c.H1.length ; i++){
            System.out.print(c.H1[i]+" ");
        }
        System.out.println("");
        for(int i = 0 ; i <c.H1.length ; i++){
            System.out.print(c.H2[i]+" ");
        }
        

    }
    
    
   
}
